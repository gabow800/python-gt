# python-gt

Hola maestra, bienvenida al proyecto.

## Comandos utiles

```bash
# historial de commits
git log --pretty=oneline

# listar todas las branches
git branch -a

# crear branch
git checkout -b <branch_name>

# subir branch
git push --set-upstream origin <branch_name>

# subir cambios
git push

```